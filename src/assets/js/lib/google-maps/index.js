import $ from 'jquery'
import googleMapsLoader from 'load-google-maps-api'
//

export const apiKey = 'AIzaSyAmjOz9vdCVJTyNqZMKTjTVelFH30oD8f0'

export const defaultMinHeight = 250
export const defaultHeight = '100%'

const ensureElementHasHeight = (
  $el,
  minHeight = defaultMinHeight,
  height = defaultHeight
) => {
  if ($el.height() <= 0) {
    $el.css({ minHeight, height })
  }
}

const readOptionsFromData = $el => {
  const lat = +$el.data('map-lat')
  const lng = +$el.data('map-lng')
  const zoom = +$el.data('map-zoom')

  if (!lat) {
    throw new Error('Please add a "data-map-lat" attribute with the latitude.')
  } else if (!lng) {
    throw new Error('Please add a "data-map-lng" attribute with the longitude.')
  } else if (!zoom) {
    throw new Error(
      'Please add a "data-map-zoom" attribute with the map zoom level.'
    )
  }

  return {
    center: {
      lat,
      lng,
    },
    zoom,
  }
}

export const googleMaps = stylePredicate => {
  $(document).ready(() => {
    const loader = googleMapsLoader({ key: apiKey })

    $('[data-map]').each((index, el) => {
      const $el = $(el)

      let styles

      if (typeof stylePredicate === 'function') {
        styles = stylePredicate($el.data('map-style'))
      }

      ensureElementHasHeight($el)

      const options = { ...readOptionsFromData($el), ...{ styles } }

      $el.each((index, el) => {
        const $el = $(el)

        loader.then(google => {
          const map = new google.Map(el, options)

          const markerOptions = {
            position: options.center,
            map,
            icon: $el.data('map-marker-url') || null,
            animation: google.Animation.DROP,
          }


          const marker = new google.Marker({ position: options.center, map, icon: "./assets/img/icon/marker_sm.png" })

          const infoWindow = new google.InfoWindow({
            content: `
<div class="map--info">
  <h3>
    Northwest Texas Healthcare System Childbirth Center  
  </h3>
  <p>
    1501 South Coulter Street<br>
    Amarillo, TX 79106 
  </p>
  <a href="https://www.google.com/maps/dir//Northwest+Texas+Healthcare+System,+1501+S+Coulter+St,+Amarillo,+TX+79106/@35.1997117,-101.9223761,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x870151b163024f3b:0x1ccdba97666473ba!2m2!1d-101.9201874!2d35.1997117" target="_blank" class="sl_button sl_button--primary">Get Directions</a>
</div>
  `,
          })

          marker.addListener('click', () => {
            infoWindow.open(map, marker)
          })
        })
      })
    })
  })
}

export default googleMaps
